// const host = "http://520686.vip:51080";
const host = "https://ai.gsk-rd-apps.com/VoltarenMini";
// const host = "https://gskchatbot.winsfa.com/VoltarenMini";

const config = {
  service :{
    host,
    // 登录地址，用于建立会话
    loginUrl: `${host}`,
    // 上传图片接口
    voiceFileUploadUrl: `${host}`,
  }
}
// const webSocketUrl = "ws://520686.vip:51080/ws/websocket";
const webSocketUrl = "wss://ai.gsk-rd-apps.com/VoltarenMini/ws/websocket";
// const webSocketUrl = "wss://gskchatbot.winsfa.com/VoltarenMini/ws/websocket";

module.exports = {
  config:config,
  host:host,
  webSocketUrl:webSocketUrl
}