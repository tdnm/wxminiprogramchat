// 检查权限列表;需要使用的权限都需要下在列表里,提示用授权;
const authList = ['scope.record'];
var authsetting = () => {
  authList.forEach(((item, index) => {
    wx.getSetting({
      success(res) {
        if (!res.authSetting[item]) {
          wx.authorize({
            scope: item,
            success() {}
          })
        }
      }
    })
  }))
}

// 检查是否有权限 再次拉起用户授权窗口,提示授权. authparm:权限参数;content:提示内容
var checkPromission = (authparm,content, callback) =>  {
  wx.getSetting({
    success(res) {
      if (!res.authSetting[authparm]) {
        wx.showModal({
          title: '警告',
          content: '您点击了拒绝授权,' + content + ',点击确定重新获取授权。',
          success: function (res) {
            if (res.confirm) {
              wx.openSetting({
                success: (res) => {
                  if (res.authSetting[authparm]) {
                    wx.authorize({
                      scope: authparm,
                      success(res) {
                        console.info('成功授权', res)
                        callback();
                      }
                    })
                  }
                }, fail: function (res) {
                  console.info('没有授权', res)
                }
              })
            }
          }
        })
      }else{
        callback();
      }
    }
  })
}

module.exports = {
  authsetting: authsetting,
  checkPromission: checkPromission
}