// pages/feedback/feedback.js
var uploadJS = require('uploadImg.js')
var util = require('../../utils/util.js')
const configJS = require('../../utils/config.js');

Page({
  data: {
    files: [],
    imageCount: 1,
    disabled: false,
    hidden: true,
    mudid:'',
    muName:'',
    email:'',
    mudidType:'0',
    qtitle:'',
    tel:'',
    detailQuestion:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      mudid: options.mudid,
      muName: options.muName,
      email: options.email,
    })
    // this.setData({
    //   mudid: '123456',
    //   muName: '111',
    //   email: 'xxxx@mail.com',
    //   mudidType: '0',
    //   qtitle: '没啥问题',
    //   tel: '18888999999',
    //   detailQuestion: '没什么问题描述',
    // })
  },
  getTitleValue:function(e){
    this.setData({
      qtitle: e.detail.value
    })
  },
  getTelValue: function (e) {
    this.setData({
      tel: e.detail.value
    })
  },
  getDetailQuestionValue: function (e) {
    this.setData({
      detailQuestion: e.detail.value
    })
  },

  chooseImage: function (e) {
    var that = this;
    wx.chooseImage({
      count: 5,
      sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        that.setData({
          files: that.data.files.concat(res.tempFilePaths)
        });
        var tempFilePaths = res.tempFilePaths;
        var imgs = that.data.files;// 需要预览的图片http链接列表
        that.setData({
          files: imgs
        });
      }
    })
  },

  // 删除图片
  deleteImg: function (e) {
    var imgs = this.data.files;
    var index = e.currentTarget.dataset.index;
    imgs.splice(index, 1);
    this.setData({
      files: imgs,
      hidden: true
    });
  },

  previewImage: function (e) {
    wx.previewImage({
      current: e.currentTarget.id, // 当前显示图片的http链接
      urls: this.data.files // 需要预览的图片http链接列表
    })
  },

  // 提交问题
  submit: function () {
    util.showBusy('发送消息中...');
    var _this = this;
    var tempFilePaths = this.data.files;
    if (!_this.data.qtitle){
      util.showModel('','问题标题必填');
      return;
    }
    if (tempFilePaths.length > 5){
      util.showModel('','最多上传5张照片.');
      return;
    }
    // 展示loading
    _this.setData({
      disabled: true
    });
    const folderName = util.uuid();
    let formData = {
      folderName: folderName,
      mudid: _this.data.mudid,
      muName: encodeURI(_this.data.muName),
      email: _this.data.email,
      mudidType: _this.data.mudidType,
      qtitle: encodeURI(_this.data.qtitle),
      tel: encodeURI(_this.data.tel),
      detailQuestion: encodeURI(_this.data.detailQuestion),
      submitSendMailUrl: configJS.config.service.host + '/miniProgram-submitSendMail', // 搞起url
    }
    uploadJS.imageUpload({
      url: configJS.config.service.host + '/miniProgram-imageUpload', //这里是你图片上传的接口
      path: tempFilePaths, //这里是选取的图片的地址数组,
    }, formData,_this);
  },
})