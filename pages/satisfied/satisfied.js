// pages/satisfied/satisfied.js
const config = require('../../utils/config.js');
const util = require('../../utils/util.js');
//获取应用实例
const app = getApp()
Page({
  data: {
    hidden: true,
    disabled: false,
    radioItems: [{
        name: '非常满意',
        value: '5',
        checked: true
      },
      {
        name: '满意',
        value: '4'
      },
      {
        name: '一般',
        value: '3'
      },
      {
        name: '不满意',
        value: '2'
      },
      {
        name: '非常不满意',
        value: '1'
      },
    ],
    service_score: '',
    inputText: "",
    taskqQueueId: '',
    mudid: '',

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log('task_queue_id', options.taskqQueueId)
    console.log('mudid', options.mudid)
    // this.setData({
    //   taskqQueueId:"201810300000000863",
    //   mudid:"8329048"
    // })
    this.setData({
      taskqQueueId: options.taskqQueueId,
      mudid: options.mudid
    })
  },

  radioChange: function(e) {
    console.log('radio发生change事件，携带value值为：', e.detail.value);

    var radioItems = this.data.radioItems;
    for (var i = 0, len = radioItems.length; i < len; ++i) {
      radioItems[i].checked = radioItems[i].value == e.detail.value;
    }

    this.setData({
      radioItems: radioItems,
      service_score: e.detail.value
    });
  },
  getInputVlaue: function(e) {
    console.log(e)
    this.setData({
      inputText: e.detail.value
    });
  },
  submit: function() {
    util.showBusy('评价中...');
    this.setData({
      disabled: true,
    })
    var radioItems = this.data.radioItems;
    let service_score = '';
    for (var i = 0, len = radioItems.length; i < len; ++i) {
      if (radioItems[i].checked) {
        service_score = radioItems[i].value;
        break;
      }
    }
    let _this = this;
    console.log(this.data.taskqQueueId, this.data.mudid, service_score, this.data.inputText, )
    wx.request({
      url: config.config.service.host + '/miniProgram-satisfied',
      data: {
        task_queue_id: this.data.taskqQueueId,
        mudid: this.data.mudid,
        service_score: service_score,
        asses: encodeURI(this.data.inputText),
      },
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded' // 默认值
      },
      success(res) {
        console.log(res);
        if (res.data && res.data.err_no == 0) {
          wx.showModal({
            title: '提示',
            content: '评价成功',
            success(res) {
              if (res.confirm) {
                wx.navigateTo({
                  url: '../index/index'
                })
              } else if (res.cancel) {
                console.log('用户点击取消')
                wx.navigateTo({
                  url: '../index/index'
                })
              }
            }
          })
        } else {
          util.showModel('评价失败了.', '相关问题请联系管理员。');
        }
      },
      complete: function() {
        wx.hideToast();
        _this.setData({
          disabled: false,
        })
      }
    })
  }
})