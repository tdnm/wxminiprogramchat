// components/rate/rate.js
const app = getApp()
const config = require('../../utils/config.js');
const util = require('../../utils/util.js');
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    value: {
      type: Number,
      value: 0
    },
    showText: {
      type: Boolean,
      value: false
    },
    tastQueueId: {
      type: String,
      value: ''
    },
    mudid: {
      type: String,
      value: ''
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    rate_num: [0, 0, 0, 0, 0],
  },

  ready() {
    this._loadData(this.data.value);
  },

  /**
   * 组件的方法列表
   */
  methods: {
    _loadData:function(valpra){
      let {
        rate_num,
        value
      } = this.data;
      value = valpra
      value > 5 && (value = 5);
      value < 0 && (value = 0);
      let arr = value.toString().split('.');
      let i = 0;
      rate_num = [0, 0, 0, 0, 0];
      while (i < arr[0]) {
        rate_num[i] = 1;
        i++;
      }

      arr[1] && (rate_num[arr[0]] = +`0.${arr[1]}`);
      this.setData({
        rate_num,
        value
      })
    },
    _scoreClick: function(e) {
      let _this = this;
      let [score, tastQueueId, mudid] = [e.currentTarget.dataset.score,
        _this.properties.tastQueueId,
        _this.properties.mudid
      ];
      // 显示分数
      this._loadData(score);
      if (!tastQueueId) {
        util.showModel('参数错误.', '相关问题请联系管理员。');
        console.error('tastQueueId空了');
        return;
      }
      if (!mudid) {
        util.showModel('参数错误.', '相关问题请联系管理员。');
        console.error('mudid空了');
        return;
      }
      util.showBusy('评价中...');
      console.log(tastQueueId, mudid, score)
      wx.request({
        url: config.config.service.host + '/miniProgram-satisfied',
        data: {
          task_queue_id: tastQueueId,
          mudid: mudid,
          service_score: score,
          asses: '',
        },
        method: 'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded' // 默认值
        },
        success(res) {
          console.log(res);
          if (res.data && res.data.err_no == 0) {
            wx.showModal({
              title: '评价成功',
              content: '请下拉刷后重新提问.',
              showCancel: false,
              success(res) {
                if (res.confirm) {
                  var myEventDetail = {
                    val: '12345'
                  }
                  _this.triggerEvent('fatherEvent', myEventDetail)
                }
              }
            })
          } else {
            util.showModel('评价失败了.', '相关问题请联系管理员。');
          }
        },
        complete: function() {
          wx.hideToast();
        }
      })
    }
  }
})